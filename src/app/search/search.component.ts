import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WikidataQueryService, IComercialBuilding } from '../core/wikidata-query.service';
import { IMapPoint } from '../map/map.component';


// https://www.wikidata.org/wiki/Wikidata:Identifiers
const WIKIDATA_IDENTIFIER_REGEXP = /https?:\/\/www\.wikidata\.org\/entity\/(\w*)/;
const COORDINATES_REGEXP = /POINT\((.*)\s(.*)\)/i;
@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent {
    public searchForm: FormGroup;
    public comercialBuildings: IComercialBuilding[] = [];

    @Output()
    public comercialBuildingsPointsChange: EventEmitter<IMapPoint[]> = new EventEmitter();

    constructor(
        private formBuilder: FormBuilder,
        private wikidataQueryService: WikidataQueryService
    ) {

    }
    ngOnInit() {
        this.wikidataQueryService.getComercialBuildings()
            .subscribe(comercialBuildings => {
                this.comercialBuildings = comercialBuildings;
            })
        this.searchForm = this.formBuilder.group({
            comercialBuilding: [null, [Validators.required]]
        })

        this.searchForm.get('comercialBuilding').valueChanges.subscribe(comercialBuildingIndex => {
            const selectedComercialBuilding = this.comercialBuildings[comercialBuildingIndex];
            const resultMatch = selectedComercialBuilding.item.value.match(WIKIDATA_IDENTIFIER_REGEXP);
            if (resultMatch) {
                const [, identifier] = resultMatch;
                this.wikidataQueryService.getComercialBuildingInstances(identifier)
                    .subscribe(comercialBuildingInstances => {
                        const comercialBuildingsPoints = comercialBuildingInstances.reduce((points: IMapPoint[], c) => {
                            if (c.coordinates?.value != null) {
                                const matchResult = c.coordinates.value.match(COORDINATES_REGEXP);
                                if (matchResult) {
                                    let [_, lon, lat ] = matchResult;
                                    points.push({
                                        metadata: c,
                                        coordinates: [Number(lon), Number(lat)]
                                    });
                                }
                            }
                            return points;
                        }, []);
                        this.comercialBuildingsPointsChange.emit(comercialBuildingsPoints);
                    })
            }
            else {
                alert('No se ha podido obtener el identificador del recurso');
                console.error('No se ha podido obtener el identificador del recurso', WIKIDATA_IDENTIFIER_REGEXP);
                console.log(selectedComercialBuilding);
            }
        })
    }
}