import { Component } from "@angular/core";
import { Map, View, Feature, Overlay } from 'ol';
import { fromLonLat, transform } from 'ol/proj'
import { defaults as defaultControls } from 'ol/control';
import { Vector as VectorLayer, Tile as TileLayer } from 'ol/layer';
import { OSM, Vector } from 'ol/source';
import { Point } from 'ol/geom';
import { Select as SelectInteraction } from 'ol/interaction';
import { IComercialBuildingInstance } from '../core/wikidata-query.service';

export type ICoordinates = [number, number];

export interface IMapPoint {
    coordinates: ICoordinates;
    metadata: any;
}
const VIEW_COORDINATES = [-8.493208, 37.843852];
@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent {
    public pointSelected: IComercialBuildingInstance;

    private pointsLayer: VectorLayer;
    private overlay: Overlay;
    private selectedPointsInteraction: SelectInteraction;
    private points: IMapPoint[] = [];
    private map: Map;

    public onComercialBuildingsPointsChange(mapPoints: IMapPoint[]) {
        this.points = mapPoints;
        this.pointSelected = null;
        this.selectedPointsInteraction && this.selectedPointsInteraction.getFeatures().clear();

        if (this.map) {
            this.map.removeLayer(this.pointsLayer);
            this.generatePointsLayer()
        }
    }

    private generatePointsLayer(mapPoints: IMapPoint[] = this.points) {

        if (Array.isArray(this.points) && this.points.length > 0) {
            this.pointsLayer = new VectorLayer({
                source: new Vector({
                    features: mapPoints.map(point => new Feature({
                        geometry: new Point(fromLonLat(point.coordinates)),
                        pointMetadata: point.metadata,
                        coordinates: point.coordinates
                    }))
                })
            });
            this.map.addLayer(this.pointsLayer);

            this.selectedPointsInteraction = new SelectInteraction({
                layers: [this.pointsLayer]
            });

            // Se ejecuta para seleccionar como para deseleccionar
            this.selectedPointsInteraction.on('select', (event) => {
                this.pointSelected = null;
                if (event.selected.length > 0) {
                    const [feature] = event.selected;
                    const { pointMetadata, coordinates } = feature.getProperties();
                    this.pointSelected = pointMetadata;
                    const newCoordiantes = transform(coordinates, 'EPSG:4326', 'EPSG:3857');
                    this.overlay.setPosition(newCoordiantes);
                }
            })

            this.map.getInteractions().extend([this.selectedPointsInteraction]);
        }
    }

    ngOnInit() {
        this.map = new Map({
            target: 'map',
            controls: defaultControls({ attribution: false }),
            layers: [
                new TileLayer({
                    source: new OSM()
                })
            ],
            view: new View({
                center: fromLonLat(VIEW_COORDINATES),
                zoom: 5
            })
        });

        this.generatePointsLayer();
    }


    ngAfterViewInit() {
        this.overlay = new Overlay({
            element: document.getElementById('overlay')
        });
        this.map.addOverlay(this.overlay);
    }
}