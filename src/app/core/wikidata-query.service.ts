import { Injectable } from "@angular/core";
import { WIKIDATA_URL } from '../app.constants';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


const QUERY_COMERCIAL_BUILDINGS = `
SELECT ?item ?itemLabel ?itemDescription WHERE {
    ?item wdt:P279 wd:Q655686.
    ?item rdfs:label ?itemLabel.
    FILTER( LANG(?itemLabel)="es" )
    SERVICE wikibase:label { bd:serviceParam wikibase:language "es,en" }
}`;

const QUERY_COMERCIAL_BUILDING_INSTANCES = (comercialBuildingIdentifier) => `
SELECT ?item ?itemLabel ?itemDescription ?coordinates ?pic WHERE {
    ?item wdt:P31/wdt:P279* wd:${comercialBuildingIdentifier}.
    ?item wdt:P625 ?coordinates.
    ?item wdt:P17 wd:Q29.
    ?item wdt:P18 ?pic.
    SERVICE wikibase:label { bd:serviceParam wikibase:language "es,en" } . 
}`;

export interface IWikidataQueryResponseData {
    type: 'uri' | 'literal' | string;
    value: string;
    'xml:lang'?: string;
    datatype?: string;
}
export interface IComercialBuilding {
    item: IWikidataQueryResponseData;
    itemDescription: IWikidataQueryResponseData;
    itemLabel: IWikidataQueryResponseData;
}

export interface IComercialBuildingInstance extends  IComercialBuilding {
    coordinates: IWikidataQueryResponseData;
    pic: IWikidataQueryResponseData;
}

export interface IWikidataQueryResponse<T> {
    head: {
        vars: string[];
    };
    results: {
        bindings: T[];
    }
}

@Injectable({
    providedIn: 'root'
})
export class WikidataQueryService {
    private readonly wikidataQueryUrl = WIKIDATA_URL;

    constructor(
        private http: HttpClient
    ){}

    getComercialBuildings(): Observable<IComercialBuilding[]> {
        return this.http.get<IWikidataQueryResponse<IComercialBuilding>>(this.wikidataQueryUrl, {params: {
            query: QUERY_COMERCIAL_BUILDINGS
        }})
            .pipe(
                map((response) => response.results.bindings)
            )
    }

    getComercialBuildingInstances(comercialBuildingIdentifier): Observable<IComercialBuildingInstance[]> {
        return this.http.get<IWikidataQueryResponse<IComercialBuildingInstance>>(this.wikidataQueryUrl, {params: {
            query: QUERY_COMERCIAL_BUILDING_INSTANCES(comercialBuildingIdentifier)
        }})
            .pipe(
                map((response) => response.results.bindings)
            )
    }
}