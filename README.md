# SemanticWeb
Demo disponible en [https://comercial-buildings-semantic-web.didream.vercel.app/](https://comercial-buildings-semantic-web.didream.vercel.app/)

Proyecto implementado con Angular 9 y OpenLayers 6, en el que se hace uso de la web semántica mediante el uso de https://query.wikidata.org/.

Dicho proyecto lista edificios comerciales en España, señalando su ubicación en un mapa. Para ello se realizan dos peticiones a https://query.wikidata.org/; la primera para obtener el listado de edificios comerciales que tengan una traducción en español.
```
SELECT ?item ?itemLabel ?itemDescription WHERE {
    ?item wdt:P279 wd:Q655686.
    ?item rdfs:label ?itemLabel.
    FILTER( LANG(?itemLabel)="es" )
    SERVICE wikibase:label { bd:serviceParam wikibase:language "es,en" }
}
```
y la segunda que, a partir de un edificio comercial seleccionado `comercialBuildingIdentifier`, sirve para obtener el listado de instancias:
```
SELECT ?item ?itemLabel ?itemDescription ?coordinates ?pic WHERE {
    ?item wdt:P31/wdt:P279* wd:${comercialBuildingIdentifier}.
    ?item wdt:P625 ?coordinates.
    ?item wdt:P17 wd:Q29.
    ?item wdt:P18 ?pic.
    SERVICE wikibase:label { bd:serviceParam wikibase:language "es,en" } . 
}
```
los cuales se mostrarán en el mapa a partir de sus coordenadas. Si se selecciona un punto del mapa se mostrará ademas información relevante como la imagen y descripción.